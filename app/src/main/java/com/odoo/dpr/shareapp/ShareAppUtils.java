package com.odoo.dpr.shareapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ShareAppUtils {

    private Context mContext;
    private String playStoreLink, message;

    public static void shareApp(Context context, String playStoreLink, String message) {
        ShareAppUtils appUtils = new ShareAppUtils(context, playStoreLink, message);
        appUtils.share();
    }

    private ShareAppUtils(Context context, String playStoreLink, String message) {
        mContext = context;
        this.playStoreLink = playStoreLink;
        this.message = message;
    }

    private void share() {

        /*
            Whatsapp not support both text and images:
            http://stackoverflow.com/a/23077592
         */
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        File appIcon = getAppIcon();
        if (appIcon != null) {
            // Adding application icon as stream
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(appIcon));
        }

        // Adding application title
        shareIntent.putExtra(Intent.EXTRA_TITLE, mContext.getString(R.string.app_name));
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.app_name));

        // Adding text with share link
        String text = (message != null) ? message + "\n" : "";
        text += playStoreLink != null ? playStoreLink : "";
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);

        // Starting share activity
        if (isSafeIntent(shareIntent)) {
            mContext.startActivity(Intent.createChooser(shareIntent, "Share app"));
        } else {
            Toast.makeText(mContext, "No application found to handle request", Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isSafeIntent(Intent intent) {
        PackageManager packageManager = mContext.getPackageManager();
        List activities = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return activities.size() > 0;
    }


    private File getAppIcon() {
        File directory = new File(Environment.getExternalStorageDirectory().getPath() + File.separator
                + "icons");
        String path = directory + File.separator + "app_icon.png";
        if (!directory.exists()) {
            if (directory.mkdirs() && !new File(path).exists()) {
                try {
                    AssetManager assetManager = mContext.getAssets();
                    InputStream in = assetManager.open("icon" + File.separator + "app_icon.png");
                    OutputStream out = new FileOutputStream(path);
                    byte[] buffer = new byte[65536 * 2];
                    int read;
                    while ((read = in.read(buffer)) != -1) {
                        out.write(buffer, 0, read);
                    }
                    out.flush();
                    in.close();
                    out.close();
                    return new File(path);
                } catch (IOException e) {
                    Log.w("Error:", "Error creating file: " + e.getMessage(), e);
                }
            }
        }
        return new File(path);
    }
}
