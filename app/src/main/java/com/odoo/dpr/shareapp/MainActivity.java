package com.odoo.dpr.shareapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnShareApp).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        /*

        1. Add large image in assets/ directory. as in this project
        2. Add permissions as in manifest
        3. Call below method.
            1. First link
            2. Any message

        Note: Some of the application does not support image and text such as whatsapp
                but in email application it will go with image and text both.


        */



        ShareAppUtils.shareApp(this, "https://play.google.com/store/apps/details?id=com.whatsapp",
                "Application share message");
    }
}
